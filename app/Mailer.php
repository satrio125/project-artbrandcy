<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use stdClass;

class Mailer extends Model
{
  private $mail;
  private $username;
  private $fromName;

  public function __construct($fromEmail, $fromName)
  {
    $this->mail = new PHPMailer(true);
    $this->mail->SMTPDebug = 2;
    $this->mail->isSMTP();
    $this->mail->Host = 'mail.artbrandcy.co.id';
    $this->mail->SMTPAuth = true;
    $this->mail->SMTPSecure = "ssl";
    $this->username = $fromEmail;
    $this->fromName = $fromName;
    $this->mail->Username = $this->username;
    $this->mail->Password = 'projectphotokerensekali!!!';
    $this->mail->Port = 465;
  }

  public function sendEmail($toEmail, $mailerName, $stdClassOfMessage)
  {
    try {
      //Recipients
      $this->mail->setFrom($this->username, $this->fromName);
      $this->mail->addAddress($toEmail, $mailerName);

      //Attachments
      // $this->mail->addAttachment('/var/tmp/file.tar.gz');              // Add attachments
      // $this->mail->addAttachment('/tmp/image.jpg', 'new.jpg');         // Optional name

      //Content
      $this->mail->isHTML(true);                                          // Set email format to HTML
      $this->mail->Subject = $stdClassOfMessage->subject;
      $this->mail->Body    = $stdClassOfMessage->HTMLMessage;
      // $this->mail->AltBody = $stdClassOfMessage->NonHTMLMessage;
      $this->mail->send();
      return "Success";
    } catch (Exception $e) {
      return "Message could not be sent. Mailer Error: ". $this->mail->ErrorInfo;
    }
  }

  public function sendEmailToStaff($stdClassOfMessage)
  {
    try {
      //Recipients
      $this->mail->setFrom($this->username, $this->fromName);
      $this->mail->addAddress('order@artbrandcy.co.id', "Artbrandcy");
      $this->mail->addCC('anggasawijaya@artbrandcy.co.id', "Anggasa Wijaya");
      $this->mail->addBCC('bowo.sma6@gmail.com', "M Satrio Wibowo");

      //Attachments
      // $this->mail->addAttachment('/var/tmp/file.tar.gz');              // Add attachments
      // $this->mail->addAttachment('/tmp/image.jpg', 'new.jpg');         // Optional name

      //Content
      $this->mail->isHTML(true);                                          // Set email format to HTML
      $this->mail->Subject = $stdClassOfMessage->subject;
      $this->mail->Body    = $stdClassOfMessage->HTMLMessage;
      // $this->mail->AltBody = $stdClassOfMessage->NonHTMLMessage;
      $this->mail->send();
      return "Success";
    } catch (Exception $e) {
      return "Message could not be sent. Mailer Error: ". $this->mail->ErrorInfo;
    }
  }
}
