<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Mailer;
use stdClass;

class Order extends Controller
{
  public function orderForm(Request $request)
  {
    return view('form_pesan', ['package' => $request->package, 'packageName' => $this->determinePackage($request->package)]);
  }

  public function determinePackage($package)
  {
    if ($package == "1x0") {
      return "Indoor A";
    } else if ($package == "1x1") {
      return "Indoor B";
    } else if ($package == "2x0") {
      return "Outdoor Lokal A";
    } else if ($package == "2x1") {
      return "Outdoor Lokal B";
    } else if ($package == "2x2") {
      return "Outdoor Lokal C";
    } else if ($package == "2x3") {
      return "Outdoor Lokal D";
    } else if ($package == "3x0") {
      return "Outdoor Mancanegara (Support Package)";
    } elseif ($package == "3x1") {
      return "Outdoor Mancanegara (Silver Package)";
    } elseif ($package == "3x2") {
      return "Outdoor Mancanegara (Gold Package)";
    } elseif ($package == "3x3") {
      return "Outdoor Mancanegara (Platinum Package)";
    } elseif ($package == "4x0") {
      return "Rebranding (New Logo Identity)";
    } elseif ($package == "4x1") {
      return "Rebranding (Rebranding Logo)";
    } elseif ($package == "4x2") {
      return "Rebranding (Branding Product)";
    }
  }

  public function notifyStaff($request)
  {
    $timestamp = Carbon::now('Asia/Jakarta')->toDateTimeString();
    $mail = new Mailer('noreply@artbrandcy.co.id', 'Artbrandcy');
    $package = $this->determinePackage($request->packageId);
    $message = new stdClass();
    $message->subject = "Ada Pesanan Baru!";
    $message->HTMLMessage = " Detail Pesanan : <br>
    Tanggal/Jam Pesan : ".$timestamp." <br>
    Jenis Paket       : ".$package." <br>
    Atas Nama         : ".$request->buyerName." <br>
    Instansi          : ".$request->shopName." <br>
    Email             : ".$request->email." <br>
    Nomor Telepon     : ".$request->phone." <br>
    Deskripsi Produk  : <br>".$request->description."<br>
    <br>
    Kepada bagian yang bertugas, silakan ditindaklanjuti dengan menghubungi customer yang bersangkutan menggunakan email order@artbrandcy.co.id";
    // $message->NonHTMLMessage = "This is a test message \n Disregard Please";
    return $mail->sendEmailToStaff($message);
  }

  public function forwardOrder(Request $request)
  {
    if ($this->notifyStaff($request) == "Success") {
      $timestamp = Carbon::now('Asia/Jakarta')->toDateTimeString();
      $mail = new Mailer('noreply@artbrandcy.co.id', 'Artbrandcy');
      $package = $this->determinePackage($request->packageId);
      $message = new stdClass();
      $message->subject = "Pesanan Sedang Kami Proses!";
      $message->HTMLMessage = "Terima kasih atas kepercayaan Anda kepada Artbrandcy. <br>
      Saat ini pesanan Anda sedang kami proses oleh tim terkait. Kami akan menghubungi Anda kembali untuk membicarakan detail dari pesanan Anda. <br><br>
      Artbrandcy | Art Branding Agency<br> <br>
      ================================================================== <br> <br>
      Berikut rincian pesanan Anda: <br>
      Tanggal/Jam       : ".$timestamp." <br>
      Jenis Paket       : ".$package." <br>
      Atas Nama         : ".$request->buyerName." <br>
      Instansi          : ".$request->shopName." <br>
      Email             : ".$request->email." <br>
      Nomor Telepon     : ".$request->phone." <br>
      Deskripsi Produk  : <br>".$request->description."<br><br>
      Email ini dikirimkan secara otomatis dan tidak menerima balasan email apapun. <br>
      Apabila Anda memiliki pertanyaan seputar pesanan yang baru saja Anda kirimkan, silakan mengirimkan email ke order@artbrandcy.co.id<br>
      ---- <br />
      Apabila Anda memiliki pertanyaan seputar produk, promosi, atau keluhan terhadap produk kami, silakan mengirimkan email ke customerservice@artbrandcy.co.id <br>";
      // $message->NonHTMLMessage = "This is a test message \n Disregard Please";
      if ($mail->sendEmail($request->email, $request->buyerName, $message) == "Success") {
        return redirect()->route('mailSuccess');
      } else {
        return redirect()->route('mailFail');
      }
    } else {
      return "Message Service is out of order. Please try again later.";
    }
  }

  public function mailSuccess()
  {
    return view('order_success');
  }

  public function mailFail()
  {
    return view('order_fail');
  }
}
