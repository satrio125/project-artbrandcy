<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    @include('head')
    <title>Paket Outdoor Mancanegara | Artbrandcy</title>
  </head>
  <body>
    @include('navbar')
    <section id="main-wrapper">
      <section id="package-details">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="row">
                <div class="col-12 selection-header">
                  Silakan Pilih Paket Foto Yang Cocok Untukmu! <br />
                  <span class="selection-header-small">(Paket Outdoor Mancanegara)</span>
                </div>
              </div>
              <div class="row">
                <div class="col-12 col-lg-3 package-list-selection-container">
                  <div class="packages-list-selection">
                    <div class="package-title">
                      Support Package
                    </div>
                    <div class="package-description">
                      Tersedia untuk 15 brand saja
                    </div>
                    <div class="package-price">
                      Mulai dari <br />
                      <span class="package-price-big">
                        <span class="colorize-purple force-bold">Rp500.000</span>
                        <small>/foto</small>
                      </span> <br />
                      <span class="package-price-small">
                        (Minimum 10 foto)
                      </span>
                    </div>
                    <div class="package-includes">
                      <span class="force-bold">Paket Termasuk</span>
                      <ul>
                        <li>1 Buku Portfolio (Edited)</li>
                      </ul>
                    </div>
                    <div class="package-button">
                      <a href="{{env('APP_URL')}}/order/3x0">
                        <span>Pesan Paket</span>
                      </a>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-lg-3 package-list-selection-container">
                  <div class="packages-list-selection">
                    <div class="package-title">
                      Silver Package
                    </div>
                    <div class="package-description">
                      Hanya untuk 5 brand!
                    </div>
                    <div class="package-description" style="font-size: 12px">
                      <b>Spot Foto</b> <br />
                      Paris, Frankfurt, Berlin, Amsterdam
                    </div>
                    <div class="package-price">
                      Mulai dari <br />
                      <span class="package-price-big">
                        <span class="colorize-purple force-bold">Rp166.667</span>
                        <small>/foto</small>
                      </span> <br />
                      <span class="package-price-small">
                        (Minimum 30 foto)
                      </span>
                    </div>
                    <div class="package-includes">
                      <span class="force-bold">Paket Termasuk</span>
                      <ul>
                        <li>1 Video Testimoni Produk (Durasi 15 detik)</li>
                        <li>1 Video Teaser Produk (Durasi 15 detik)</li>
                      </ul>
                    </div>
                    <div class="package-button">
                      <a href="{{env('APP_URL')}}/order/3x1">
                        <span>Pesan Paket</span>
                      </a>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-lg-3 package-list-selection-container">
                  <div class="packages-list-selection">
                    <div class="package-title">
                      Gold Package
                    </div>
                    <div class="package-description">
                      Hanya untuk 5 brand!
                    </div>
                    <div class="package-description" style="font-size: 12px">
                      <b>Spot Foto</b> <br />
                      Paris, Frankfurt, Berlin, Amsterdam
                    </div>
                    <div class="package-price">
                      Mulai dari <br />
                      <span class="package-price-big">
                        <span class="colorize-purple force-bold">Rp133.333</span>
                        <small>/foto</small>
                      </span> <br />
                      <span class="package-price-small">
                        (Minimum 60 foto)
                      </span>
                    </div>
                    <div class="package-includes">
                      <span class="force-bold">Paket Termasuk</span>
                      <ul>
                        <li>1 Buku Portfolio Produk (Edited)</li>
                        <li>3 Video Teaser Produk (Durasi 15 detik)</li>
                        <li>2 Video Testimoni Produk (Durasi 15 detik)</li>
                      </ul>
                    </div>
                    <div class="package-button">
                      <a href="{{env('APP_URL')}}/order/3x2">
                        <span>Pesan Paket</span>
                      </a>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-lg-3 package-list-selection-container">
                  <div class="packages-list-selection">
                    <div class="package-title">
                      Platinum Package
                    </div>
                    <div class="package-description">
                      Hanya untuk 5 brand!
                    </div>
                    <div class="package-description" style="font-size: 12px">
                      <b>Spot Foto</b> <br />
                      London, Paris, Frankfurt, Berlin, Amsterdam
                    </div>
                    <div class="package-price">
                      Mulai dari <br />
                      <span class="package-price-big">
                        <span class="colorize-purple force-bold">Rp150.000</span>
                        <small>/foto</small>
                      </span> <br />
                      <span class="package-price-small">
                        (Minimum 100 foto)
                      </span>
                    </div>
                    <div class="package-includes">
                      <span class="force-bold">Paket Termasuk</span>
                      <ul>
                        <li>1 Video Testimoni Produk (Durasi 1 Menit)</li>
                        <li>1 Video Teaser Produk</li>
                        <li>1 Video Profil Produk (Durasi 2 Menit)</li>
                        <li>Trading Produk dengan UK / EU</li>
                      </ul>
                    </div>
                    <div class="package-button">
                      <a href="{{env('APP_URL')}}/order/3x3">
                        <span>Pesan Paket</span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </section>
    @include('footer')
  </body>
</html>
