<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    @include('head')
    <title>Selamat!</title>
  </head>
  <body>
    @include('navbar')
    <section id="main-wrapper">
      <div class="container">
        <div class="row">
          <div class="col-12 window-kotak-form">
            <div class="desc-title">
              Hore, pesanan Anda sedang kami proses!
            </div>
            <div class="desc-description">
              <div class="desc-message">
                <p>
                  Tim kami akan segera menghubungi Anda paling lambat 2x24 jam di hari kerja <br>
                  melalui email atau nomor telepon yang Anda berikan. <br>
                  Pastikan email dan nomor telepon Anda aktif.
                  <br><br>
                  Apabila Anda tidak menerima pesan kami dalam 2x24 jam di hari kerja, <br>
                  mohon periksa folder spam pada email Anda.
                  <br> <br>
                  Apabila Anda ingin bertanya tentang pesanan Anda kepada kami, <br> silakan menghubungi kami via email <a href="mailto:order@artbrandcy.co.id">order@artbrandcy.co.id</a><br><br>
                  Terima kasih telah mempercayakan layanan foto produk Anda kepada Artbrandcy!
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    @include('footer')
  </body>
</html>
