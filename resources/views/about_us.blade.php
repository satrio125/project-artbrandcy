<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    @include('head')
    <title>Tentang Kami | Artbrandcy</title>
  </head>
  <body>
    @include('navbar')
    <section id="main-wrapper">
      <section id="about-us">
        <div class="container-fluid" id="topbar" style="background-image: url('{{env('APP_URL')}}/images/header2.jpg')">
          <div class="topbar-absolute">
            <div class="topbar-absolute-title">
              We Are Here to Make Your Product More Valuable.
            </div>
            <div class="topbar-absolute-caption">
              Kami percaya bahwa penampilan produk merupakan faktor utama yang mempengaruhi penjualan produk Anda. Baik penjualan secara offline maupun secara online. Artbrandcy hadir untuk membantu Anda memasarkan produk lebih baik dari sebelumnya dengan foto, video, dan branding produk terbaik. Didukung oleh fotografer dan editor yang berpengalaman di bidangnya, kami siap membawa produk Anda ke level yang lebih tinggi.
            </div>
          </div>
        </div>
        <div class="container">
          <!-- <div class="row">
            <div class="col-12">
              <div class="intro">
                <h1 class="text-center">Tentang Artbrandcy</h1>
              </div>
            </div>
          </div> -->
          <div class="row">
            <div class="col-12 col-md-4 text-center order-md-8">
              <div class="row">
                <div class="col-12">
                  <div class="row">
                    <div class="ceo-picture">
                      <div class="ceo-image" style="background-image: url('{{env('APP_URL')}}/images/angga.jpg')">

                      </div>
                    </div>
                    <div class="ceo-caption">
                      <div class="ceo-name">
                        Anggasa Wijaya
                      </div>
                      <div class="ceo-title">
                        Co-Founder Artbrandcy
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-md-8 order-md-1">
              <div class="text">
                <h2>Artbrandcy</h2>
                <p>
                  Artbrandcy hadir menjadi sahabat korporasi maupun UMKM (Usaha Mikro, Kecil, dan Menengah) dalam membentuk “brand image” pemasaran yang eﬁsien, dengan memanfaatkan unsur bisnis, artistik, inovasi, dan kreatiﬁtas di setiap program-programnya. <br /> <br />
                  Artbrandcy sampai saat ini sudah dipercaya menangani foto product <i>indoor</i>, <i>outdoor</i>, video profil perusahaan atau institusi, foto profil individu atau korporasi secara digital di beberapa kawasan paling berpengaruh di dunia seperti London (UK), Paris, Brussels, Amsterdam, Frankfurt-Berlin di Jerman dan berbagai kawasan di Asia seperti Jepang, Hongkong, Indonesia.
                </p>
              </div>
              <div class="text">
                <h2>Background</h2>
                <p>
                  Pesatnya pertumbuhan Wirausaha baru di &nbsp;Indonesia khususnya di kota besar mempengaruhi selera pasar terhadap suatu produk karna persaingan yang semakin variatif, membuat wirausaha harus semakin kreatif dan inovatif mengemas sebuah citra brand agar nilai jual semakin tinggi dan dapat mempengaruhi besarnya market share bisnis.&nbsp;<br><br>Seperti yang kita ketahui, tidak semua wirausaha mempunyai tenaga ahli dalam pemasaran juga brand development. Selain untuk membentuk citra positif, hal ini merupakan cara mengekspresikan, memperkenalkan, dan membentuk seperti apa perusahaanya.&nbsp;
                </p>
              </div>
              <div class="text">
                <h2>Resources</h2>
                <p>
                  Artbrandcy dibangun dengan team yang Solid dan Co-Founders yang sudah memiliki pengalaman baik dalam kemampuan bisnis maupun branding. <br /> <br />
                  Co-Founder Anggasa Wijaya merupakan lulusan dari <i>MBA Program Creative &amp; Cultural Enterpreneurship</i> Insitut Teknologi Bandung dan juga Goldsmith London University (UK). Co-Founder lainnya bernama Harry Kurniawan merupakan lulusan dari German University dengan gelar sarjana bisnis di bidang Marketing dan Brand Development yang telah berpengalaman selama 6 tahun di Frankfurt sebagai Brand Development, Video/Photography, dan UI/UX &amp; Creative Designer.
                </p>
              </div>
              <div class="text team-list">
                <h2>Team</h2>
                <div class="col-12 text-center">
                  <div class="row">
                    <div class="col-12">
                      <div class="team-container">
                        <div class="ceo-picture">
                          <div class="ceo-image" style="background-image: url('{{env('APP_URL')}}/images/angga.jpg')">

                          </div>
                        </div>
                        <div class="ceo-caption">
                          <div class="ceo-name">
                            Anggasa Wijaya
                          </div>
                          <div class="ceo-title">
                            Co-Founder Artbrandcy
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-4">
                      <div class="team-container">
                        <div class="ceo-picture">
                          <div class="ceo-image" style="background-image: url('{{env('APP_URL')}}/images/team/sam.jpg')">

                          </div>
                        </div>
                        <div class="ceo-caption">
                          <div class="ceo-name">
                            Sami F.
                          </div>
                          <div class="ceo-title">
                            Business Development
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-4">
                      <div class="team-container">
                        <div class="ceo-picture">
                          <div class="ceo-image" style="background-image: url('{{env('APP_URL')}}/images/team/gatot.jpg')">

                          </div>
                        </div>
                        <div class="ceo-caption">
                          <div class="ceo-name">
                            Gatot Adriansyah
                          </div>
                          <div class="ceo-title">
                            Professional Photographer
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-4">
                      <div class="team-container">
                        <div class="ceo-picture">
                          <div class="ceo-image" style="background-image: url('{{env('APP_URL')}}/images/team/sofan.jpg')">

                          </div>
                        </div>
                        <div class="ceo-caption">
                          <div class="ceo-name">
                            Sofan Ibnu M.
                          </div>
                          <div class="ceo-title">
                            Professional Videographer
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-4 col-lg-3">
                      <div class="team-container">
                        <div class="ceo-picture">
                          <div class="ceo-image" style="background-image: url('{{env('APP_URL')}}/images/team/Agung.jpg')">
                          </div>
                        </div>
                        <div class="ceo-caption">
                          <div class="ceo-name">
                            Agung Jati
                          </div>
                          <div class="ceo-title">
                           CEO
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-4 col-lg-3">
                      <div class="team-container">
                        <div class="ceo-picture">
                          <div class="ceo-image" style="background-image: url('{{env('APP_URL')}}/images/team/Rofif.jpeg')">
                          </div>
                        </div>
                        <div class="ceo-caption">
                          <div class="ceo-name">
                            Rofif Irsyad
                          </div>
                          <div class="ceo-title">
                            Marketing Team
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-4 col-lg-3">
                      <div class="team-container">
                        <div class="ceo-picture">
                          <div class="ceo-image" style="background-image: url('{{env('APP_URL')}}/images/team/Iqbal.jpg')">
                          </div>
                        </div>
                        <div class="ceo-caption">
                          <div class="ceo-name">
                            M. Iqbal
                          </div>
                          <div class="ceo-title">
                            Marketing Team
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-4 col-lg-3">
                      <div class="team-container">
                        <div class="ceo-picture">
                          <div class="ceo-image" style="background-image: url('{{env('APP_URL')}}/images/team/Hafizhuddin.jpg')">
                          </div>
                        </div>
                        <div class="ceo-caption">
                          <div class="ceo-name">
                            Hafizhuddin
                          </div>
                          <div class="ceo-title">
                            Marketing Team
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-4 col-lg-3">
                      <div class="team-container">
                        <div class="ceo-picture">
                          <div class="ceo-image" style="background-image: url('{{env('APP_URL')}}/images/team/Nicho.jpg')">
                          </div>
                        </div>
                        <div class="ceo-caption">
                          <div class="ceo-name">
                            I. Eunicho S.
                          </div>
                          <div class="ceo-title">
                            Marketing Team
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-4 col-lg-3">
                      <div class="team-container">
                        <div class="ceo-picture">
                          <div class="ceo-image" style="background-image: url('{{env('APP_URL')}}/images/team/Aqmal.jpg')">
                          </div>
                        </div>
                        <div class="ceo-caption">
                          <div class="ceo-name">
                            Aqmal Insan C.
                          </div>
                          <div class="ceo-title">
                            Social Media Specialist
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-4 col-lg-3">
                      <div class="team-container">
                        <div class="ceo-picture">
                          <div class="ceo-image" style="background-image: url('{{env('APP_URL')}}/images/team/Rifel.jpg')">
                          </div>
                        </div>
                        <div class="ceo-caption">
                          <div class="ceo-name">
                            Rifel Noer
                          </div>
                          <div class="ceo-title">
                            Design Team
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-4 col-lg-3">
                      <div class="team-container">
                        <div class="ceo-picture">
                          <div class="ceo-image" style="background-image: url('{{env('APP_URL')}}/images/team/Afif.JPG')">
                          </div>
                        </div>
                        <div class="ceo-caption">
                          <div class="ceo-name">
                            Afif Darmawan
                          </div>
                          <div class="ceo-title">
                            Design Team
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-4 col-lg-3">
                      <div class="team-container">
                        <div class="ceo-picture">
                          <div class="ceo-image" style="background-image: url('{{env('APP_URL')}}/images/team/Ayyub.jpg')">
                          </div>
                        </div>
                        <div class="ceo-caption">
                          <div class="ceo-name">
                            M. S. Al Ayyubi
                          </div>
                          <div class="ceo-title">
                            Design Team
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-4 col-lg-3">
                      <div class="team-container">
                        <div class="ceo-picture">
                          <div class="ceo-image" style="background-image: url('{{env('APP_URL')}}/images/team/Nero.jpg')">
                          </div>
                        </div>
                        <div class="ceo-caption">
                          <div class="ceo-name">
                            Nero Chaniago
                          </div>
                          <div class="ceo-title">
                            Design Team
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-4 col-lg-3">
                      <div class="team-container">
                        <div class="ceo-picture">
                          <div class="ceo-image" style="background-image: url('{{env('APP_URL')}}/images/team/Prima.jpg')">
                          </div>
                        </div>
                        <div class="ceo-caption">
                          <div class="ceo-name">
                            Moch. Prima M.
                          </div>
                          <div class="ceo-title">
                            Design Team
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-4 col-lg-3">
                      <div class="team-container">
                        <div class="ceo-picture">
                          <div class="ceo-image" style="background-image: url('{{env('APP_URL')}}/images/team/io.jpg')">
                          </div>
                        </div>
                        <div class="ceo-caption">
                          <div class="ceo-name">
                            M. Satrio W.
                          </div>
                          <div class="ceo-title">
                            Web-Development Team
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-4 col-lg-3">
                      <div class="team-container">
                        <div class="ceo-picture">
                          <div class="ceo-image" style="background-image: url('{{env('APP_URL')}}/images/team/Naufal.jpg')">
                          </div>
                        </div>
                        <div class="ceo-caption">
                          <div class="ceo-name">
                            Naufal Fanny
                          </div>
                          <div class="ceo-title">
                            Web-Development Team
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-4 col-lg-3">
                      <div class="team-container">
                        <div class="ceo-picture">
                          <div class="ceo-image" style="background-image: url('{{env('APP_URL')}}/images/team/IqbalDandy.jpg')">
                          </div>
                        </div>
                        <div class="ceo-caption">
                          <div class="ceo-name">
                            Iqbal Dandy L.
                          </div>
                          <div class="ceo-title">
                            Web-Development Team
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-4 col-lg-3">
                      <div class="team-container">
                        <div class="ceo-picture">
                          <div class="ceo-image" style="background-image: url('{{env('APP_URL')}}/images/team/Bayu.JPG')">
                          </div>
                        </div>
                        <div class="ceo-caption">
                          <div class="ceo-name">
                            Bayu Pramana
                          </div>
                          <div class="ceo-title">
                            Web-Development Team
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-4 col-lg-3">
                      <div class="team-container">
                        <div class="ceo-picture">
                          <div class="ceo-image" style="background-image: url('{{env('APP_URL')}}/images/team/Elza.JPG')">
                          </div>
                        </div>
                        <div class="ceo-caption">
                          <div class="ceo-name">
                            Elza Fitria
                          </div>
                          <div class="ceo-title">
                            Web-Development Team
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-4 col-lg-3">
                      <div class="team-container">
                        <div class="ceo-picture">
                          <div class="ceo-image" style="background-image: url('{{env('APP_URL')}}/images/team/Atika.jpeg')">
                          </div>
                        </div>
                        <div class="ceo-caption">
                          <div class="ceo-name">
                            Atika Oktavia
                          </div>
                          <div class="ceo-title">
                            Web-Development Team
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="text" id="call">
                <h2>Hubungi Kami</h2>
                <ul style="list-style-type:disc">
                  <li>+62 228 2004 764</li>
                  <li>+62 819 3355 5845</li>
                  <li>Email <a href="mailto:customerservice@artbrandcy.co.id">customerservice@artbrandcy.co.id</a></li>
                  <li>Instagram <a href="https://www.instagram.com/artbrandcy/">ArtBrandcy</a></li>
                </ul>
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </section>
    @include('footer')
  </body>
</html>
