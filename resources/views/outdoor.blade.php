<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    @include('head')
    <title>Paket Foto Outdoor (Non-studio) | Artbrandcy</title>
  </head>
  <body>
    @include('navbar')
    <section id="main-wrapper">
      <section id="package-details">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="row">
                <div class="col-12">
                  <div class="row packages-list-container-long">
                    <div class="col-12 col-md-3 package-preview-big bg-pos-right" style="background-image: url('{{env('APP_URL')}}/images/outdoorlocal/outloc2.JPG');"></div>
                    <div class="col-12 col-md-9">
                      <div class="row">
                        <div class="col-4 package-preview-item" style="background-image: url('{{env('APP_URL')}}/images/outdoorlocal/outloc.jpg')">

                        </div>
                        <div class="col-4 package-preview-item" style="background-image: url('{{env('APP_URL')}}/images/outdoorlocal/outloc4.JPG')">

                        </div>
                        <div class="col-4 package-preview-item"style="background-image: url('{{env('APP_URL')}}/images/outdoorlocal/outloc9.jpg')">

                        </div>
                      </div>
                      <div class="row package-description">
                        <div class="col-12">
                          <div class="desc-title">
                            Paket Foto Outdoor Lokal
                          </div>
                        </div>
                        <div class="col-12">
                          <div class="desc-description">
                            Setelah anda, mendapatkan pelayanan terbaik kami untuk foto produk singkat anda secara Indoor, sebaiknya anda mencoba untuk Product Campaign di Luar Studio bersama Model dan Tim terbaik kami. Selain membuat brand image anda meningkat, konsumen anda dihadirkan dengan konsep foto dan Video produk menarik yang dapat membuat penjualan meningkat dengan “konsep komersialitas, Product Campaign yang berbeda setiap session dari tempat, konsep dan model yang menyatu dalam ide pemasaran terbaik untuk anda pemilik bisnis”. Konfirmasi ke tim kami, untuk tau paket outdoor lokal terdekat.
                            <a href="{{env('APP_URL')}}/packages/outdoor_a">
                              <div class="desc-button">
                                Pilih Paket
                              </div>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row packages-list-container-long">
                    <div class="col-12 col-md-3 package-preview-big" style="background-image: url('{{env('APP_URL')}}/images/outdmanca/distasiunlondon.jpg')"></div>
                    <div class="col-12 col-md-9">
                      <div class="row">
                        <div class="col-4 package-preview-item" style="background-image: url('{{env('APP_URL')}}/images/outdmanca/bungkusinter.jpg')">

                        </div>
                        <div class="col-4 package-preview-item" style="background-image: url('{{env('APP_URL')}}/images/outdmanca/orangdiparis.jpg')">

                        </div>
                        <div class="col-4 package-preview-item"style="background-image: url('{{env('APP_URL')}}/images/outdmanca/tasdikreta.jpg')">

                        </div>
                      </div>
                      <div class="row package-description">
                        <div class="col-12">
                          <div class="desc-title">
                            Paket Foto Outdoor Mancanegara
                          </div>
                        </div>
                        <div class="col-12">
                          <div class="desc-description">
                            Setelah berhasil melakukan “product campaign di Landmarked London, Paris, Brussels, Belgium, Amsterdam, Roma, Berlin, Frankfurt, Spanyol, Rusia, Jepang, Korea, China, Singapore, San Fransisco, New York City dengan artisan Internasional, dalam membuat konten Dunia untuk Produk Lokal Indonesia. Setiap tahun kami mempunyai beberapa Project Mancanegara, yang melibatkan model dengan Latar Landmarked Dunia untuk Foto dan Video Produk bisnis anda. Terbayang bagaimana Brand Image produk akan meningkat yang berpengaruh keputusan konsumen yang pada akhirnya akan membeli produk anda. Hub Tim Kami untuk mengetahui, Projek Outdoor Mancanegara terdekat, yang akan kami lakukan bersama Artis, Model dan Tim terbaik kami.
                            <a href="{{env('APP_URL')}}/packages/outdoor_b">
                              <div class="desc-button">
                                Pilih Paket
                              </div>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </section>
    @include('footer')
  </body>
</html>
