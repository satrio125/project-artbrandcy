<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    @include('head')
    <title>Galeri | Artbrandcy</title>
  </head>
  <body>
    @include('navbar')
    <section id="main-wrapper">
      <section id="gallery">
        <section id="gallery-toptext">
          <div class="container">
            <div class="row">
              <div class="col-12">
                <div class="row">

                </div>
              </div>
            </div>
          </div>
        </section>
        <section id="gallery-item">
          <div class="container">
            <div class="row photo-item-container">
              <div class="col-4 photo-item">
                <div class="image" style="background-image: url('{{env('APP_URL')}}/images/portfolio/porto19.jpg');">

                </div>
              </div>
              <div class="col-4 photo-item">
                <div class="image" style="background-image: url('{{env('APP_URL')}}/images/portfolio/porto32.JPG');">

                </div>
              </div>
              <div class="col-4 photo-item">
                <div class="image" style="background-image: url('{{env('APP_URL')}}/images/portfolio/porto35.JPG');">

                </div>
              </div>
            </div>
            <div class="row photo-item-container">
              <div class="col-4 photo-item">
                <div class="image" style="background-image: url('{{env('APP_URL')}}/images/portfolio/porto38.JPG');">

                </div>
              </div>
              <div class="col-4 photo-item">
                <div class="image" style="background-image: url('{{env('APP_URL')}}/images/portfolio/porto27.JPG');">

                </div>
              </div>
              <div class="col-4 photo-item">
                <div class="image" style="background-image: url('{{env('APP_URL')}}/images/portfolio/porto21.jpg');">

                </div>
              </div>
            </div>

            <div class="row photo-item-container">
              <div class="col-4 photo-item">
                <div class="image" style="background-image: url('{{env('APP_URL')}}/images/portfolio/porto11.JPG');">

                </div>
              </div>
              <div class="col-4 photo-item">
                <div class="image" style="background-image: url('{{env('APP_URL')}}/images/portfolio/porto12.JPG');">

                </div>
              </div>
              <div class="col-4 photo-item">
                <div class="image" style="background-image: url('{{env('APP_URL')}}/images/portfolio/portofolio13.JPG');">

                </div>
              </div>
            </div>

            <div class="row photo-item-container">
              <div class="col-4 photo-item">
                <div class="image" style="background-image: url('{{env('APP_URL')}}/images/portfolio/porto29.jpg');">

                </div>
              </div>
              <div class="col-4 photo-item">
                <div class="image" style="background-image: url('{{env('APP_URL')}}/images/portfolio/porto30.jpg');">

                </div>
              </div>
              <div class="col-4 photo-item">
                <div class="image" style="background-image: url('{{env('APP_URL')}}/images/portfolio/porto31.jpg');">

                </div>
              </div>
            </div>

            <div class="row photo-item-container">
              <div class="col-4 photo-item">
                <div class="image" style="background-image: url('{{env('APP_URL')}}/images/portfolio/porto1.JPG');">

                </div>
              </div>
              <div class="col-4 photo-item">
                <div class="image" style="background-image: url('{{env('APP_URL')}}/images/portfolio/porto2.JPG');">

                </div>
              </div>
              <div class="col-4 photo-item">
                <div class="image" style="background-image: url('{{env('APP_URL')}}/images/portfolio/porto5.JPG');">

                </div>
              </div>
            </div>

            <div class="row photo-item-container">
              <div class="col-4 photo-item">
                <div class="image" style="background-image: url('{{env('APP_URL')}}/images/portfolio/porto3.JPG');">

                </div>
              </div>
              <div class="col-4 photo-item">
                <div class="image" style="background-image: url('{{env('APP_URL')}}/images/portfolio/porto4.JPG');">

                </div>
              </div>
              <div class="col-4 photo-item">
                <div class="image" style="background-image: url('{{env('APP_URL')}}/images/portfolio/porto6.JPG');">

                </div>
              </div>
            </div>

            <div class="row photo-item-container">
              <div class="col-4 photo-item">
                <div class="image" style="background-image: url('{{env('APP_URL')}}/images/portfolio/porto18.jpg');">

                </div>
              </div>
              <div class="col-4 photo-item">
                <div class="image" style="background-image: url('{{env('APP_URL')}}/images/portfolio/porto17.jpg');">

                </div>
              </div>
              <div class="col-4 photo-item">
                <div class="image" style="background-image: url('{{env('APP_URL')}}/images/portfolio/porto10.JPG');">

                </div>
              </div>
            </div>

          </div>
        </section>
      </section>
    </section>
    @include('footer')
  </body>
</html>
