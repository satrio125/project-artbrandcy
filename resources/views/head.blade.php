<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="{{env('APP_URL')}}/css/app.css">
<link rel="stylesheet" href="{{env('APP_URL')}}/css/main.css">
<link rel="icon" href="{{env('APP_URL')}}/images/logo.png">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<script src="{{env('APP_URL')}}/js/app.js" charset="utf-8"></script>
