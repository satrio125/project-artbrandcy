<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    @include('head')
    <title>Paket Indoor | Artbrandcy</title>
  </head>
  <body>
    @include('navbar')
    <section id="main-wrapper">
      <section id="package-details">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="row">
                <div class="col-12">
                  <div class="row packages-list-container-long">
                    <div class="col-12 col-md-3 package-preview-big" style="background-image: url('{{env('APP_URL')}}/images/paket_indoor/indoor_Picture1.jpg')"></div>
                    <div class="col-12 col-md-9">
                      <div class="row">
                        <div class="col-4 package-preview-item" style="background-image: url('{{env('APP_URL')}}/images/paket_indoor/indoor_Picture2.jpg')">

                        </div>
                        <div class="col-4 package-preview-item" style="background-image: url('{{env('APP_URL')}}/images/paket_indoor/indoor_Picture7.jpg')">

                        </div>
                        <div class="col-4 package-preview-item"style="background-image: url('{{env('APP_URL')}}/images/paket_indoor/indoor_Picture4.jpg')">

                        </div>
                      </div>
                      <div class="row package-description">
                        <div class="col-12">
                          <div class="desc-title">
                            Paket Foto Indoor A
                          </div>
                        </div>
                        <div class="col-12">
                          <div class="desc-description">
                            Penawaran terbatas dengan harga special Rp.80.000/Foto sudah termasuk (Model, Studio, Fotograpi dan Editing) dengan minimum 5 Foto /Transaksi. Anda bisa mengirimkan produk anda ke kami, dan akan dikembalikan kembali setelah foto session selesai. Dalam waktu singkat, hasil foto terbaik akan kami kirim ke anda. Manfaatkan kesempatan ini, untuk menjadi “produk terbaik” di Industri bisnis anda.<br>
                            (harga tidak termasuk ongkos kirim dan pengembalian)
                            <a href="{{env('APP_URL')}}/order/1x0">
                              <div class="desc-button">
                                Pesan Paket
                              </div>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row packages-list-container-long">
                    <div class="col-12 col-md-3 package-preview-big" style="background-image: url('{{env('APP_URL')}}/images/paket_indoor/indoor_picture200.jpg')"></div>
                    <div class="col-12 col-md-9">
                      <div class="row">
                        <div class="col-4 package-preview-item" style="background-image: url('{{env('APP_URL')}}/images/paket_indoor/indoor_Picture6.JPG')">

                        </div>
                        <div class="col-4 package-preview-item bg-pos-bottom" style="background-image: url('{{env('APP_URL')}}/images/paket_indoor/indoor_Picture3.jpg')">

                        </div>
                        <div class="col-4 package-preview-item"style="background-image: url('{{env('APP_URL')}}/images/paket_indoor/indoor_Piccture9.JPG')">

                        </div>
                      </div>
                      <div class="row package-description">
                        <div class="col-12">
                          <div class="desc-title">
                            Paket Foto Indoor B
                          </div>
                        </div>
                        <div class="col-12">
                          <div class="desc-description">
                            Penawaran Terbatas Paket “upgrade indoor” untuk anda, yang menginginkan foto produk “20 foto” yang banyak dengan bonus “video teaser” 15 detik, tentunya dengan harga paket yang sangat terjangkau senilai Rp.2.000.000 sudah termasuk (Model, Studio, Fotograpi dan Editing).
                            <a href="{{env('APP_URL')}}/order/1x1">
                              <div class="desc-button">
                                Pesan Paket
                              </div>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </section>
    @include('footer')
  </body>
</html>
