<nav>
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="logo">
          <a href="{{env('APP_URL')}}">
            <img src="{{env('APP_URL')}}/images/AB-Horizontal.png">
          </a>
        </div>
        <ul>
          <a href="{{env('APP_URL')}}"><li>Beranda</li></a>
          <a href="{{env('APP_URL')}}/gallery"><li>Galeri</li></a>
          <a href="{{env('APP_URL')}}/packages"><li>Paket</li></a>
          <a href="{{env('APP_URL')}}/models"><li>Model</li></a>
          <a href="{{env('APP_URL')}}/about_us"><li>Tentang Artbrandcy</li></a>
        </ul>
        <div class="mobile-toggle">
          <i class="fas fa-bars"></i>
        </div>
      </div>
    </div>
  </div>
</nav>
<div class="mobile-menu">
  <ul>

  </ul>
</div>
<div class="back-to-top">
  <i class="fas fa-arrow-up"></i>
</div>

<script type="text/javascript">
  $(".mobile-menu ul").html("<a href='#' class='close-menu'><li>X</li><a>"+$("nav ul").html());

  $(".mobile-toggle").on("click", function() {
    $(".mobile-menu").show();
  });

  $(".close-menu").on("click", function() {
    $(".mobile-menu").hide();
  })

  $(".back-to-top").on('click', function() {
    $("html, body").animate({
      scrollTop: 0
    }, 500);
  })

  $(window).scroll(function() {
    var height = $(window).scrollTop();
    if (height > 200) {
      $(".back-to-top").fadeIn();
    } else {
      $(".back-to-top").fadeOut();
    }
  });
</script>
