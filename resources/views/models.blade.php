<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    @include('head')
    <title>Galeri Model | Artbrandcy</title>
  </head>
  <body>
    @include('navbar')
    <section id="main-wrapper">
      <section id="models">
        <div class="container">
          <div class="row">
            <div class="col-12 selection-header">
              Model yang bekerjasama dengan Artbrandcy<br />
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <div class="row">
                <div class="col-12 col-md-6 model-col">
                  <div class="model-container">
                    <div class="model-image">

                    </div>
                    <div class="model-desc">
                      Clarissa
                    </div>
                  </div>
                </div>
                <div class="col-12 col-md-6 model-col">
                  <div class="model-container">
                    <div class="model-image">

                    </div>
                    <div class="model-desc">
                      Erlangga
                    </div>
                  </div>
                </div>
                <div class="col-12 col-md-6 model-col">
                  <div class="model-container">
                    <div class="model-image">

                    </div>
                    <div class="model-desc">
                      Nirina
                    </div>
                  </div>
                </div>
                <div class="col-12 col-md-6 model-col">
                  <div class="model-container">
                    <div class="model-image">

                    </div>
                    <div class="model-desc">
                      Biruma
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </section>
    @include('footer')
  </body>
</html>
