<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    @include('head')
    <title>Paket Artbrandcy | Artbrandcy</title>
  </head>
  <body>
    @include('navbar')
    <section id="main-wrapper">
      <section id="package-tab">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="row">
                <div class="col-12 col-md-4">
                  <div class="packages-list-container">
                    <div class="package-image" style="background-image: url('{{env('APP_URL')}}/images/fotopaket/indoor.JPG')">

                    </div>
                    <div class="package-description">
                      <div class="desc-title">
                        Paket Foto Indoor
                      </div>
                      <div class="desc-description">
                        Paket Foto Produk Indoor untuk Anda khusus para “Startup Bisnis” dengan fasilitas model lokal dan internasional, diambil dalam studio oleh fotograper senior berpengalaman, difinalisasi dengan editing yang matang oleh tenaga professional.
                      </div>
                      <a class="desc-button-link" href="{{env('APP_URL')}}/packages/indoor">
                        <div class="desc-button">
                          Lihat Pilihan Paket
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-md-4">
                  <div class="packages-list-container">
                    <div class="package-image" style="background-image: url('{{env('APP_URL')}}/images/fotopaket/outdoor.jpg')">

                    </div>
                    <div class="package-description">
                      <div class="desc-title">
                        Paket Foto Outdoor
                      </div>
                      <div class="desc-description">
                        Setelah berhasil lebih dari 3 tahun melakukan projek International ke Britania Raya, Europian Union , dan Asia bersama brand lokal Indonesia, kami kembali membuka untuk Anda para “startup bisnis”, Paket Foto Produk Outdoor Lokal Indonesia dan Internasional dengan sudah termasuk fasilitas model lokal dan internasional, bayangkan “produk anda, berada di tengah-tengah latar landmarked Dunia”, di dukung  oleh fotograper senior berpengalaman, difinalisasi dengan editing yang matang oleh tenaga professional.
                      </div>
                      <a class="desc-button-link" href="{{env('APP_URL')}}/packages/outdoor">
                        <div class="desc-button">
                          Lihat Pilihan Paket
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-md-4">
                  <div class="packages-list-container">
                    <div class="package-image" style="background-image: url('{{env('APP_URL')}}/images/fotopaket/rebranding.JPG')">

                    </div>
                    <div class="package-description">
                      <div class="desc-title">
                        Paket Rebranding
                      </div>
                      <div class="desc-description">
                        Penawaran terbatas, yang khusus kami fasilitasi untuk anda yang ingin membuat, memperbaiki dan menambah “value” dalam merek bisnis anda. Mulai dari membuat logo, marketing tools sampai dengan visualisasi merek dengan panduan “Ide Konsep Anda” atau dengan “Ide Tim Terbaik kami” dalam mencapai tujuan “value” sesuai visi perusahaan anda.
                      </div>
                      <a class="desc-button-link" href="{{env('APP_URL')}}/packages/rebranding">
                        <div class="desc-button">
                          Lihat Pilihan Paket
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </section>
    @include('footer')
  </body>
</html>
