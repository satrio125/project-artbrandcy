<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    @include('head')
    <title>Form Pemesanan | Artbrandcy</title>
  </head>
  <body>
    @include('navbar')
    <section id="main-wrapper" style="position: relative;">
      <section id="package-details">
        <div class="container">
          <div class="row">
            <div class="col-12 window-kotak-form">
              <div class="desc-title">
                Tinggal selangkah lagi!
              </div>
              <div class="desc-description">
                <div class="desc-message">
                  Silakan isikan data diri Anda dan deskripsi produk milik Anda, kami akan segera menghubungi Anda segera melalui email / nomor telepon yang Anda berikan. <br />
                  <b style="font-weight: 600">
                    Khusus Periode ini! Dapatkan diskon sampai dengan 30% dari harga paket yang Anda pilih khusus untuk pemesanan melalui web!
                  </b>
                </div>
                <br />
                <div class="order-form">
                  <form action="{{env('APP_URL')}}/order/aa" method="post">
                    {{csrf_field()}}
                    <input type="text" name="packageId" value="{{$package}}" hidden="hidden">
                    <div class="group">
                      <input type="text" name="buyerName" required="required" /><span class="highlight"></span><span class="bar"></span>
                      <label>Nama Pemesan</label>
                    </div>
                    <div class="group">
                      <input type="text" name="shopName" required="required" /><span class="highlight"></span><span class="bar"></span>
                      <label>Instansi / Nama Usaha</label>
                    </div>
                    <div class="group">
                      <input type="text" name="email" required="required" /><span class="highlight"></span><span class="bar"></span>
                      <label>Email</label>
                    </div>
                    <div class="group">
                      <input type="text" name="phone" required="required" /><span class="highlight"></span><span class="bar"></span>
                      <label>Nomor Telepon</label>
                    </div>
                    <div class="group">
                      <textarea type="text" name="description" required="required" row="5"></textarea><span class="highlight"></span><span class="bar"></span>
                      <label>Deskripsi Produk</label>
                    </div>
                    <button class="desc-button-link" href="{{env('APP_URL')}}/packages/rebranding" type="submit">
                      <div class="desc-button">
                        Pesan Paket <br>
                        <b>{{$packageName}}</b>
                      </div>
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </section>
    @include('footer')
  </body>
</html>
