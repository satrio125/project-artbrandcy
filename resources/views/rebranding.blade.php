<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    @include('head')
    <title>Paket Rebranding | Artbrandcy</title>
  </head>
  <body>
    @include('navbar')
    <section id="main-wrapper">
      <section id="package-details">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="row">
                <div class="col-12">
                  <div class="row packages-list-container-long">
                    <div class="col-12 col-md-3 package-preview-big" style="background-image: url('{{env('APP_URL')}}/images/rebranding/newlogo1.JPG')"></div>
                    <div class="col-12 col-md-9">
                      <div class="row">
                        <div class="col-4 package-preview-item" style="background-image: url('{{env('APP_URL')}}/images/rebranding/newlogo2.JPG')">

                        </div>
                        <div class="col-4 package-preview-item" style="background-image: url('{{env('APP_URL')}}/images/rebranding/newlogo3.JPG')">

                        </div>
                        <div class="col-4 package-preview-item"style="background-image: url('{{env('APP_URL')}}/images/rebranding/newlogo.JPG')">

                        </div>
                      </div>
                      <div class="row package-description">
                        <div class="col-12">
                          <div class="desc-title">
                            New Logo Identity
                          </div>
                        </div>
                        <div class="col-12">
                          <div class="desc-description">
                            Konsumen akan mendapatkan 2 tahap sebelum mendapatkan logo.
                            Untuk tahap 1, analisis basic bisnis Segemetasi –Positioning dan Targetting sampai Filosofi bisnis, produk ,
                            visi perusahaan, juga business model yang akan membantu tim kami menentukan logo apa yang sesuai, dalam bentuk story board.
                            Sehingga menghasilkan beberapa refrensi logo, yang akan kami tawarkan kepada konsumen,
                            dan konsumen diberikan 1 tahap terakhir berupa revisi dari hasil logo yang kami tawarkan.
                            <ul style="list-style-type:disc">
                              <li>Paket New Logo untuk UKM mulai dari Rp.700.000/logo </li>
                            </ul>
                            <a href="{{env('APP_URL')}}/order/4x0">
                              <div class="desc-button">
                                Pesan Paket
                              </div>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row packages-list-container-long">
                    <div class="col-12 col-md-3 package-preview-big" style="background-image: url('{{env('APP_URL')}}/images/rebranding/rebranding1.JPG')"></div>
                    <div class="col-12 col-md-9">
                      <div class="row">
                        <div class="col-4 package-preview-item" style="background-image: url('{{env('APP_URL')}}/images/rebranding/rebranding2.JPG')">


                        </div>
                        <div class="col-4 package-preview-item" style="background-image: url('{{env('APP_URL')}}/images/rebranding/rebranding3.JPG')">


                        </div>
                        <div class="col-4 package-preview-item" style="background-image: url('{{env('APP_URL')}}/images/rebranding/rebranding5.jpg')">


                        </div>
                      </div>
                      <div class="row package-description">
                        <div class="col-12">
                          <div class="desc-title">
                            Rebranding Logo
                          </div>
                        </div>
                        <div class="col-12">
                          <div class="desc-description">
                            Rebranding Brand, mulai dari logo brand, membuat marketing tools , konsultasi bisnis, sampai dengan membuat perubahan rencana bisnis.
                            Cocok buat anda, yang merasa “brand identity” bisnis kurang kuat dimata konsumen.
                            Dan ingin mengembangkanya agar sesuai dengan kondisi terbaru industry bisnis anda.
                            Idealnya inovasi “brand logo”, harus di upgrade 3 tahun sekali, dan selalu bertransformasi tanpa menghilangkan “pakem” logo dan identity di awal.
                            <ul style="list-style-type:disc">
                              <li>Paket Rebranding product mulai dari Rp.1.000.000 (be confirmed)</li>
                            </ul>
                            <a href="{{env('APP_URL')}}/order/4x1">
                              <div class="desc-button">
                                Pesan Paket
                              </div>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row packages-list-container-long">
                    <div class="col-12 col-md-3 package-preview-big" style="background-image: url('{{env('APP_URL')}}/images/rebranding/branding3.JPG')"></div>
                    <div class="col-12 col-md-9">
                      <div class="row">
                        <div class="col-4 package-preview-item"style="background-image: url('{{env('APP_URL')}}/images/rebranding/sandals.JPG')">


                        </div>
                        <div class="col-4 package-preview-item" style="background-image: url('{{env('APP_URL')}}/images/rebranding/branding2.JPG')">


                        </div>
                        <div class="col-4 package-preview-item" style="background-image: url('{{env('APP_URL')}}/images/rebranding/branding1.JPG')">


                        </div>
                      </div>
                      <div class="row package-description">
                        <div class="col-12">
                          <div class="desc-title">
                            Branding Product
                          </div>
                        </div>
                        <div class="col-12">
                          <div class="desc-description">
                            Branding Product sama dengan Foto Produk.
                            Di paket branding product sifatnya lebih customized disesuaikan dengan STP Perusahaan,
                            Business Model dan Marketing Mix sebelumnya. Dari panduan itu, kami bantu custom produk anda.
                            <ul style="list-style-type:disc">
                              <li>Paket Rebranding product mulai dari Rp.1.000.000 (be confirmed)</li>
                            </ul>
                            <a href="{{env('APP_URL')}}/order/4x2">
                              <div class="desc-button">
                                Pesan Paket
                              </div>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </section>
    @include('footer')
  </body>
</html>
