<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    @include('head')
    <title>Artbrandcy | Increase Your Own Goal</title>
  </head>
  <body>
    @include('navbar')
    <section id="main-wrapper">
      <section id="top">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="row">
                <div class="header-image" style="background-image: url('{{env('APP_URL')}}/images/header2.jpg');">
                  <div class="header-text">
                    <div class="big-text">
                      <b>ArtBrandcy</b>
                    </div>
                    <div class="small-text">
                      Layanan Foto dan Video Produk di berbagai lokasi terkenal di Dunia.
                    </div>
                    <div class="button">
                      <a href="#package">Lihat Paket</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section id="package">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="row">
                <div class="section-title">
                  Paket Artbrandcy
                </div>
              </div>
              <div class="row">
                <div class="col-12 col-md-3 package-item" style="background-image: url('{{env('APP_URL')}}/images/portfolio7.jpg')">
                  <div class="overlay-package-item">
                    <div class="text-item">
                      Paket Indoor
                      <a href="{{env('APP_URL')}}/packages/indoor">
                        <div class="text-button">
                          Lihat Paket
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-md-3 package-item" style="background-image: url('{{env('APP_URL')}}/images/portfolio8.jpg')">
                  <div class="overlay-package-item">
                    <div class="text-item">
                      Paket Outdoor Lokal
                      <a href="{{env('APP_URL')}}/packages/outdoor_a">
                        <div class="text-button">
                          Lihat Paket
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-md-3 package-item" style="background-image: url('{{env('APP_URL')}}/images/portfolio12.jpg')">
                  <div class="overlay-package-item">
                    <div class="text-item">
                      Paket Outdoor Mancanegara
                      <a href="{{env('APP_URL')}}/packages/outdoor_b">
                        <div class="text-button">
                          Lihat Paket
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-md-3 package-item" style="background-image: url('{{env('APP_URL')}}/images/portfolio2.jpg')">
                  <div class="overlay-package-item">
                    <div class="text-item">
                      Paket Rebranding
                      <a href="{{env('APP_URL')}}/packages/rebranding">
                        <div class="text-button">
                          Lihat Paket
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section id="portfolio">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="row">
                <div class="section-title">
                  Beberapa Portfolio Artbrandcy
                </div>
              </div>
              <div class="row no-gutters">
                <div class="col-4">
                  <img src="{{env('APP_URL')}}/images/portfolio1.jpg">
                  <img src="{{env('APP_URL')}}/images/portfolio2.jpg">
                  <img src="{{env('APP_URL')}}/images/portfolio3.jpg">
                </div>
                <div class="col-4">
                  <img src="{{env('APP_URL')}}/images/portfolio4.jpg">
                  <img src="{{env('APP_URL')}}/images/portfolio5.jpg">
                  <img src="{{env('APP_URL')}}/images/portfolio10.jpg">
                </div>
                <div class="col-4">
                  <img src="{{env('APP_URL')}}/images/portfolio7.jpg">
                  <img src="{{env('APP_URL')}}/images/portfolio8.jpg">
                  <img src="{{env('APP_URL')}}/images/portfolio9.jpg">
                  <img src="{{env('APP_URL')}}/images/portfolio6.jpg">
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </section>
    @include('footer')
  </body>
</html>
