<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    @include('head')
    <title>Paket Foto Outdoor Lokal | Artbrandcy</title>
  </head>
  <body>
    @include('navbar')
    <section id="main-wrapper">
      <section id="package-details">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="row">
                <div class="col-12 selection-header">
                  Silakan Pilih Paket Foto Yang Cocok Untukmu! <br />
                  <span class="selection-header-small">(Paket Outdoor Lokal)</span>
                </div>
              </div>
              <div class="row">
                <div class="col-12 col-md-3 package-list-selection-container">
                  <div class="packages-list-selection">
                    <div class="package-title">
                      Outdoor A
                    </div>
                    <div class="package-description">
                      Cocok untuk UMKM yang baru memulai usaha!
                    </div>
                    <div class="package-price">
                      Mulai dari <br />
                      <span class="package-price-big">
                        <span class="colorize-purple force-bold">Rp300.000</span>
                        <small>/foto</small>
                      </span> <br />
                      <span class="package-price-small">
                        (Minimum 10 foto)
                      </span>
                    </div>
                    <div class="package-includes">
                      <span class="force-bold">Paket Termasuk</span>
                      <ul>
                        <li>1 Buku Portfolio (Edited)</li>
                      </ul>
                    </div>
                    <div class="package-button">
                      <a href="{{env('APP_URL')}}/order/2x0">
                        <span>Pesan Paket</span>
                      </a>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-md-3 package-list-selection-container">
                  <div class="packages-list-selection">
                    <div class="package-title">
                      Outdoor B
                    </div>
                    <div class="package-description">
                      Paket Hemat Untuk Toko Besar
                    </div>
                    <div class="package-price">
                      Mulai dari <br />
                      <span class="package-price-big">
                        <span class="colorize-purple force-bold">Rp240.000</span>
                        <small>/foto</small>
                      </span> <br />
                      <span class="package-price-small">
                        (Minimum 25 foto)
                      </span>
                    </div>
                    <div class="package-includes">
                      <span class="force-bold">Paket Termasuk</span>
                      <ul>
                        <li>1 Buku Portfolio (Edited)</li>
                      </ul>
                    </div>
                    <div class="package-button">
                      <a href="{{env('APP_URL')}}/order/2x1">
                        <span>Pesan Paket</span>
                      </a>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-md-3 package-list-selection-container">
                  <div class="packages-list-selection">
                    <div class="package-title">
                      Outdoor C
                    </div>
                    <div class="package-description">
                      Cocok Untuk Tingkat Pemasaran Menengah!
                    </div>
                    <div class="package-price">
                      Mulai dari <br />
                      <span class="package-price-big">
                        <span class="colorize-purple force-bold">Rp250.000</span>
                        <small>/foto</small>
                      </span> <br />
                      <span class="package-price-small">
                        (Minimum 40 foto)
                      </span>
                    </div>
                    <div class="package-includes">
                      <span class="force-bold">Paket Termasuk</span>
                      <ul>
                        <li>1 Video Teaser Produk (Durasi 15 detik)</li>
                      </ul>
                    </div>
                    <div class="package-button">
                      <a href="{{env('APP_URL')}}/order/2x2">
                        <span>Pesan Paket</span>
                      </a>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-md-3 package-list-selection-container">
                  <div class="packages-list-selection">
                    <div class="package-title">
                      Outdoor D
                    </div>
                    <div class="package-description">
                      Untuk UMKM dengan Pasar yang Luas!
                    </div>
                    <div class="package-price">
                      Mulai dari <br />
                      <span class="package-price-big">
                        <span class="colorize-purple force-bold">Rp600.000</span>
                        <small>/foto</small>
                      </span> <br />
                      <span class="package-price-small">
                        (Minimum 25 foto)
                      </span>
                    </div>
                    <div class="package-includes">
                      <span class="force-bold">Paket Termasuk</span>
                      <ul>
                        <li>2 Video Testimoni Produk (Durasi 15 detik)</li>
                        <li>1 Video Teaser Produk (Durasi 45 detik)</li>
                      </ul>
                    </div>
                    <div class="package-button">
                      <a href="{{env('APP_URL')}}/order/2x3">
                        <span>Pesan Paket</span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </section>
    @include('footer')
  </body>
</html>
