<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return view('index');
});

Route::get('/packages/indoor', function() {
  return view('indoor');
});

Route::get('/packages/outdoor', function() {
  return view('outdoor');
});

Route::get('/packages/outdoor_a', function() {
  return view('outdoor_a');
});

Route::get('/packages/outdoor_b', function() {
  return view('outdoor_b');
});

Route::get('/packages/rebranding', function() {
  return view('rebranding');
});

Route::get('/gallery', function() {
  return view('gallery');
});

Route::get('/packages', function() {
  return view('package');
});

Route::get('/models', function() {
  return view('models');
});

Route::get('/order/{package}', ['uses' => 'Order@orderForm']);

Route::get('/order/finished/success', ['uses' => 'Order@mailSuccess', 'as' => 'mailSuccess']);
Route::get('/order/finished/fail', ['uses' => 'Order@mailFail', 'as' => 'mailFail']);

Route::post('/order/{package}', ['uses' => 'Order@forwardOrder']);

Route::get('/about_us', function() {
  return view('about_us');
});
